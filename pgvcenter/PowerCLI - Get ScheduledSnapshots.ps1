﻿#Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm 
#Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $true

Connect-VIServer -Server pgvcenter.stanford.edu

#(Get-View ScheduledTaskManager).ScheduledTask | %{ (Get-View $_).Info } | Export-Csv -Path C:\temp\GetScheduledTasks.csv -NoTypeInformation -UseCulture
(Get-View ScheduledTaskManager).ScheduledTask | ForEach-Object { (Get-View $_).Info } | Select-Object Name,Description,TaskObject,Enabled,State,NextRunTime | Export-Csv -Path C:\temp\GetScheduledTasks.csv -NoTypeInformation -UseCulture

Disconnect-VIServer -Server pgvcenter.stanford.edu -Confirm:$false
Start-Sleep -s 60
