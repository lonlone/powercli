﻿#Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm 
Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $true

Connect-VIServer -Server pgvcenter.stanford.edu


Get-VM | Select-Object Name,PowerState,
    @{N="Tools Installed";E={$_.Guest.ToolsVersion -ne ""}},
    @{N="Tools Status";E={$_.ExtensionData.Guest.ToolsStatus}},
    @{N="Tools version";E={if($_.Guest.ToolsVersion -ne ""){$_.Guest.ToolsVersion}}} | Where-Object { $_.PowerState -eq "PoweredOn" } | Export-Csv -Path C:\temp\VMWareToolsStatus.csv -NoTypeInformation -UseCulture

Disconnect-VIServer -Server pgvcenter.stanford.edu -Confirm:$false
Start-Sleep -s 60