﻿#Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm 
#Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $true

###############
# These are the values you should get from your webform
#
$fileName = 'C:\Temp\GetSnapshots.csv'
$vcName = 'pgvcenter.stanford.edu'
###############

Connect-VIServer -Server $vcName

Import-Csv -Path $fileName -UseCulture | ForEach-Object {
    
    $VMName = $_.VM
    $snapName = $_.Name

    if ($VMName) {

        Write-Host "VMName: $VMName"

        $vm = Get-VM -Name $VMName 
        $snapshot = Get-Snapshot -VM $vm -Name $snapName

        Remove-Snapshot -Snapshot $snapshot -Confirm:$false
    }
}

Disconnect-VIServer -Server $vcName -Confirm:$false
Start-Sleep -s 60