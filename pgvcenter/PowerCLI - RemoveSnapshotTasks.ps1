﻿#Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm 
Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $true

Connect-VIServer -Server pgvcenter.stanford.edu

$task = ((Get-View ScheduledTaskManager).ScheduledTask | ForEach-Object { (Get-View $_).Info } | Where-Object {$_.Description -eq "Winter 2019 Patching"})
$task.RemoveScheduledTask

Disconnect-VIServer -Server pgvcenter.stanford.edu -Confirm:$false
Start-Sleep -s 60