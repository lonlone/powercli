﻿#Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm 
Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $true

Connect-VIServer -Server pgvcenter.stanford.edu

Get-VM | Select-Object Name,VMHost,PowerState | Where-Object { $_.PowerState -eq "PoweredOn" } | Export-Csv -Path C:\temp\PoweredOnVMsAndHosts.csv -NoTypeInformation -UseCulture

Disconnect-VIServer -Server pgvcenter.stanford.edu -Confirm:$false
Start-Sleep -s 60