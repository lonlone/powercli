﻿#Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm 
Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $true

###############
# These are the values you should get from your webform
#
$snapMemory = $false
$snapQuiesce = $true
$emailAddr = 'postgrads-sysadm@lists.stanford.edu'
$fileName = 'C:\Temp\snapshotScheduler.csv'
$vcName = 'pgvcenter.stanford.edu'
###############

Connect-VIServer -Server $vcName

Import-Csv -Path $fileName -UseCulture | ForEach-Object {
    
    $dateString = $_.SnapDateTime
    $snapTime = Get-Date $dateString
    $snapName = $_.SnapName
    $snapDescription = $_.SnapDescription
    $VMName = $_.VMName
    $Description = $_.SnapDescription

    Write-Host "VMName: $VMName"

    $vm = Get-VM -Name $VMName 
    $si = Get-View ServiceInstance 

    $scheduledTaskManager = Get-View $si.Content.ScheduledTaskManager

    $spec = New-Object VMware.Vim.ScheduledTaskSpec
    $spec.Name = "Snapshot",$vm.Name -join ' '
    $spec.Description = $Description
    $spec.Enabled = $true
    $spec.Notification = $emailAddr

    $spec.Scheduler = New-Object VMware.Vim.OnceTaskScheduler
    $spec.Scheduler.runat = $snapTime
   
    $spec.Action = New-Object VMware.Vim.MethodAction
    $spec.Action.Name = "CreateSnapshot_Task"

    @($snapName,$snapDescription,$snapMemory,$snapQuiesce) | ForEach-Object {
        $arg = New-Object VMware.Vim.MethodActionArgument
        $arg.Value = $_
        $spec.Action.Argument += $arg
    }

    $scheduledTaskManager.CreateObjectScheduledTask($vm.ExtensionData.MoRef, $spec)
}

Disconnect-VIServer -Server $vcName -Confirm:$false
Start-Sleep -s 60
