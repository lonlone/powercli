Add-Type -assembly System.Windows.Forms

Function msgbox ($msg) {
    [System.Windows.Forms.MessageBox]::Show($msg)
}

Function clear_results() {
    $txt_results.Text = ""
}

Function add_to_results($msg) {
    $txt_results.Text += $msg + "`r`n"
}

Function connect_vcenter() {
    if ($null -eq $cbo_vcenter.SelectedItem) {
        $lbl_vcenterstat.Text = "Error: Select a vCenter Server before Connecting"
    } else {
        if (Connect-VIServer -Server $cbo_vcenter.SelectedItem -ErrorAction SilentlyContinue) {
            $lbl_vcenterstat.Text = "Connected to " + $cbo_vcenter.SelectedItem
            $cbo_vcenter.Enabled = $false    
        } else {
            $lbl_vcenterstat.Text = "Error: Login Failed"
        }
    }
}

Function disconnect_vcenter() {
    if ($null -ne $cbo_vcenter.SelectedItem) {
        Disconnect-VIServer -Server $cbo_vcenter.SelectedItem -Confirm:$false
        $lbl_vcenterstat.Text = "Disconnected from " + $cbo_vcenter.SelectedItem
        $cbo_vcenter.Enabled = $true
    }
}

Function get_initdir() {
    $exportdir = $lbl_fileexport.Text
    if ($exportdir -ne "" -And $null -ne $exportdir) {
        return "$exportdir"
    } else {
        return "[Environment]::GetFolderPath('Documents')"
    }

}

Function select_fileexport() {
    $exportbrowser = New-Object System.Windows.Forms.FolderBrowserDialog 
    $null = $exportbrowser.ShowDialog()
    $lbl_fileexport.Text = $exportbrowser.SelectedPath
}

Function is_connected() {
    if ($lbl_vcenterstat.Text.StartsWith("Connected to ")){
        return $true
    } else {
        add_to_results "Not connected to vCenter.  Connect first."
        return $false
    }
}

Function has_fileexport() {
    if ($lbl_fileexport.Text -ne "" -And $null -ne $lbl_fileexport.Text) {
        return $true
    } else {
        add_to_results "No Export Directory specified.  Please Select an Export Folder first."
        return $false
    }
}

Function export_hosts() {
    clear_results
    add_to_results "Export VM Hosts - Processing...`r`n"

    if (-not(is_connected)) { return }
    if (-not(has_fileexport)) { return }
    try {
        $export = $lbl_fileexport.Text + '\PoweredOnHostsAndVMs - ' + $cbo_vcenter.SelectedItem + '.csv'
        Get-VM | Select-Object Name,VMHost,PowerState | Where-Object { $_.PowerState -eq "PoweredOn" } | Export-Csv -Path $export -NoTypeInformation -UseCulture
        add_to_results "Exported VM Hosts to: "
        add_to_results $export
        add_to_results "Export VM Hosts - Complete!"
    } catch {
        $ErrorMessage = $_.Exception.Message
        add_to_results "Error Message: $ErrorMessage"
    }
}

Function export_specs() {
    clear_results
    add_to_results "Export VM Specs - Processing...`r`n"

    if (-not(is_connected)) { return } 
    if (-not(has_fileexport)) { return }

    try {
        $export = $lbl_fileexport.Text + '\VMSpecs - ' + $cbo_vcenter.SelectedItem + '.csv'
        Get-VM | Select-Object Name,ProvisionedSpaceGB,UsedSpaceGB,NumCPU,CoresPerSocket,MemoryMB,HardwareVersion,VMHost,PowerState | Where-Object { $_.PowerState -eq "PoweredOn" } | Export-Csv -Path $export -NoTypeInformation -UseCulture
        add_to_results "Exported VM Specs to: " 
        add_to_results $export 
        add_to_results "Export VM Specs - Complete!"
    } catch {
        $ErrorMessage = $_.Exception.Message
        add_to_results "Error Message: $ErrorMessage"
    }
}

Function export_vmtools() {
    clear_results
    add_to_results "Export VM Tools - Processing...`r`n"

    if (-not(is_connected)) { return }
    if (-not(has_fileexport)) { return }

    try {
        $export = $lbl_fileexport.Text + '\VMTools - ' + $cbo_vcenter.SelectedItem + '.csv'
        Get-VM | Select-Object Name,PowerState,
            @{N="Tools Installed";E={$_.Guest.ToolsVersion -ne ""}},
            @{N="Tools Status";E={$_.ExtensionData.Guest.ToolsStatus}},
            @{N="Tools version";E={if($_.Guest.ToolsVersion -ne ""){$_.Guest.ToolsVersion}}} | Where-Object { $_.PowerState -eq "PoweredOn" } | Export-Csv -Path $export -NoTypeInformation -UseCulture
        add_to_results "Exported VM Tools to: " 
        add_to_results $export 
        add_to_results "Export VM Tools - Complete!"
    } catch {
        $ErrorMessage = $_.Exception.Message
        add_to_results "Error Message: $ErrorMessage"
    }
}

Function create_snaptasks() {
    clear_results
    add_to_results "Create Snapshot Tasks - Processing...`r`n"
    if (-not(is_connected)) { return }
    $initdir = get_initdir
    $snaptasks_browser = New-Object System.Windows.Forms.OpenFileDialog -Property @{ 
        InitialDirectory = $initdir 
        Filter = 'Comma Separate Values(*.csv)|*.csv'
    }
    $snaptasks_browser.ShowDialog()
    
    $snapMemory = $false
    $snapQuiesce = $true
    $emailAddr = $txt_createsnaptasks.Text
    $fileName = $snaptasks_browser.Filename

    if ($null -ne $fileName -And $fileName -ne "") {
        Import-Csv -Path $fileName -UseCulture | ForEach-Object {
            try {
                $dateString = $_.SnapDateTime
                $snapTime = Get-Date $dateString
                $snapName = $_.SnapName
                $snapDescription = $_.SnapDescription
                $VMName = $_.VMName

                $vm = Get-VM -Name $VMName 
                $si = Get-View ServiceInstance 

                $scheduledTaskManager = Get-View $si.Content.ScheduledTaskManager

                $spec = New-Object VMware.Vim.ScheduledTaskSpec
                $spec.Name = "$snapDescription - Snapshot for ",$vm.Name -join ''
                $spec.Description = $snapDescription
                $spec.Enabled = $true
                $spec.Notification = $emailAddr

                $spec.Scheduler = New-Object VMware.Vim.OnceTaskScheduler
                $spec.Scheduler.runat = $snapTime
   
                $spec.Action = New-Object VMware.Vim.MethodAction
                $spec.Action.Name = "CreateSnapshot_Task"

                @($snapName,$snapDescription,$snapMemory,$snapQuiesce) | ForEach-Object {
                    $arg = New-Object VMware.Vim.MethodActionArgument
                    $arg.Value = $_
                    $spec.Action.Argument += $arg
                }
                $scheduledTaskManager.CreateObjectScheduledTask($vm.ExtensionData.MoRef, $spec)

                add_to_results "Snapshot Task Created: ",$spec.Name -join ''
                add_to_results "Scheduled for: $snapTime"
                add_to_results "Email Notification sent to: $emailAddr `r`n"
            } catch {
                $ErrorMessage = $_.Exception.Message
                add_to_results "Error Message: $ErrorMessage"
            }
        }
        add_to_results "Create Snapshot Tasks - Complete!"
    } else {
        add_to_results "No Snapshot Tasks creation File selected.  Cancelling..."
    }
}

Function get_snapshots() {
    clear_results
    add_to_results "Get Snapshots - Processing...`r`n"

    if (-not(is_connected)) { return }
    if (-not(has_fileexport)) { return }

    try {
        $fileName = $lbl_fileexport.Text + '\' + $cbo_vcenter.SelectedItem + ' - Snapshots.csv'
        Get-VM | Get-Snapshot | Select-Object vm,name,description,created,sizegb | Export-Csv -Path $fileName -NoTypeInformation -UseCulture
        add_to_results "Snapshots written to: " 
        add_to_results $fileName
        add_to_results "Get Snapshots - Complete!"
    } catch {
        $ErrorMessage = $_.Exception.Message
        add_to_results "Error Message: $ErrorMessage"
    }
}

Function del_snapshots() {
    clear_results
    add_to_results "Delete Snapshots - Processing...`r`n"
    if (-not(is_connected)) { return }

    $initdir = get_initdir
    $snapshots_browser = New-Object System.Windows.Forms.OpenFileDialog -Property @{ 
        InitialDirectory = $initdir 
        Filter = 'Comma Separate Values(*.csv)|*.csv'
    }
    $snapshots_browser.ShowDialog()

    $fileName = $snapshots_browser.Filename

    if ($null -ne $fileName -And $fileName -ne "") {
        Import-Csv -Path $fileName -UseCulture | ForEach-Object {
            try {
                $VMName = $_.VM
                $snapName = $_.Name
                if ($VMName) {
                    $vm = Get-VM -Name $VMName 
                    $snapshot = Get-Snapshot -VM $vm -Name $snapName
                    Remove-Snapshot -Snapshot $snapshot -Confirm:$false

                    add_to_results "VM: $VMName"
                    add_to_results "Snapshot Deleted: $snapName `r`n"
                }
            } catch {
                $ErrorMessage = $_.Exception.Message
                add_to_results "Error Message: $ErrorMessage"
            }
        }
        add_to_results "Delete Snapshots - Complete!"
    } else {
        add_to_results "No Delete Snapshot File selected.  Cancelling..."
    }
}

Function get_snaptasks() {
    clear_results
    add_to_results "Get Snapshot Tasks - Processing...`r`n"
    if (-not(is_connected)) { return }
    if (-not(has_fileexport)) { return }

    try {
        $fileName = $lbl_fileexport.Text + '\' + $cbo_vcenter.SelectedItem + ' - Snapshot Tasks.csv'
        (Get-View ScheduledTaskManager).ScheduledTask | ForEach-Object { (Get-View $_).Info } | Select-Object Name,Description,TaskObject,Enabled,State,NextRunTime | Export-Csv -Path $fileName -NoTypeInformation -UseCulture
        add_to_results "Snapshot Tasks written to: " 
        add_to_results $fileName
        add_to_results "Get Snapshot Tasks - Complete!"
    } catch {
        $ErrorMessage = $_.Exception.Message
        add_to_results "Error Message: $ErrorMessage"
    }
}

Function del_snaptasks() {
    clear_results
    add_to_results "Delete Snapshot Tasks - Processing...`r`n"
    if (-not(is_connected)) { return }

    $initdir = get_initdir
    $snaptasks_browser = New-Object System.Windows.Forms.OpenFileDialog -Property @{ 
        InitialDirectory = $initdir 
        Filter = 'Comma Separate Values(*.csv)|*.csv'
    }
    $snaptasks_browser.ShowDialog()

    $fileName = $snaptasks_browser.Filename

    if ($null -ne $fileName -And $fileName -ne "") {
        Import-Csv -Path $fileName -UseCulture | ForEach-Object {
            try {
                $SnapName = $_.Name
        
                $SI = Get-View ServiceInstance
                $scheduledTaskManager = Get-View $si.Content.ScheduledTaskManager
        
                $Task = Get-View -Id $scheduledTaskManager.ScheduledTask | Where-Object { $_.Info.Name -eq $SnapName}
                if ($null -ne $Task) {
                    $Task.RemoveScheduledTask()
                    $task_name = $Task.Info.Name
                    add_to_results "Removed $task_name"
                }
            } catch {
                $ErrorMessage = $_.Exception.Message
                add_to_results "Error Message: $ErrorMessage"
            }
        }
        add_to_results "Delete Snapshot Tasks - Complete!"
    } else {
        add_to_results "No Delete Snapshot Tasks File selected.  Cancelling..."
    }
}

$main_form = New-Object System.Windows.Forms.Form
$main_form.Text = 'VMWare Management'
$main_form.Width = 600
$main_form.Height = 715
$main_form.AutoSize = $false
$main_form.FormBorderStyle = 'Fixed3D'
$main_form.MaximizeBox = $false

$lbl_vcenter = New-Object System.Windows.Forms.Label
$lbl_vcenter.Text = "Select vCenter Server: "
$lbl_vcenter.Location = New-Object System.Drawing.Point (10,15)
$lbl_vcenter.AutoSize = $true
$main_form.Controls.Add($lbl_vcenter)

$cbo_vcenter = New-Object System.Windows.Forms.ComboBox
$cbo_vcenter.Width = 180
$cbo_vcenter.Items.Add("asvcprd03.stanford.edu")
$cbo_vcenter.Items.Add("asoodvcprd01.stanford.edu")
$cbo_vcenter.Items.Add("pgvcenter.stanford.edu")
$cbo_vcenter.Location = New-Object System.Drawing.Point(140,10)
$main_form.Controls.Add($cbo_vcenter)

$btn_vcenterconn = New-Object System.Windows.Forms.Button
$btn_vcenterconn.Location = New-Object System.Drawing.Point(340,8)
$btn_vcenterconn.Size = New-Object System.Drawing.Size(100,23)
$btn_vcenterconn.Text = "Connect"
$btn_vcenterconn.Add_Click({connect_vcenter})
$main_form.Controls.Add($btn_vcenterconn)

$btn_vcenterdconn = New-Object System.Windows.Forms.Button
$btn_vcenterdconn.Location = New-Object System.Drawing.Point(450,8)
$btn_vcenterdconn.Size = New-Object System.Drawing.Size(100,23)
$btn_vcenterdconn.Text = "Disconnect"
$btn_vcenterdconn.Add_Click({disconnect_vcenter})
$main_form.Controls.Add($btn_vcenterdconn)

$lbl_vcenterstat = New-Object System.Windows.Forms.Label
$lbl_vcenterstat.Text = "Not Connected"
$lbl_vcenterstat.Location = New-Object System.Drawing.Point (10, 40)
$lbl_vcenterstat.AutoSize = $true
$main_form.Controls.Add($lbl_vcenterstat)

$btn_fileexport = New-Object System.Windows.Forms.Button
$btn_fileexport.Location = New-Object System.Drawing.Point(10,80)
$btn_fileexport.Size = New-Object System.Drawing.Size(150,23)
$btn_fileexport.Text = "Select Export Folder"
$btn_fileexport.Add_Click({select_fileexport})
$main_form.Controls.Add($btn_fileexport)

$lbl_fileexport = New-Object System.Windows.Forms.Label
$lbl_fileexport.Location = New-Object System.Drawing.Point(190, 84)
$lbl_fileexport.AutoSize = $true
$main_form.Controls.Add($lbl_fileexport)

$btn_exporthosts = New-Object System.Windows.Forms.Button
$btn_exporthosts.Location = New-Object System.Drawing.Point (10, 110)
$btn_exporthosts.Size = New-Object System.Drawing.Size(150,23)
$btn_exporthosts.Text = "Export VM Hosts"
$btn_exporthosts.Add_Click({export_hosts})
$main_form.Controls.Add($btn_exporthosts)

$btn_exportspecs = New-Object System.Windows.Forms.Button
$btn_exportspecs.Location = New-Object System.Drawing.Point (170, 110)
$btn_exportspecs.Size = New-Object System.Drawing.Size(150,23)
$btn_exportspecs.Text = "Export VM Specs"
$btn_exportspecs.Add_Click({export_specs})
$main_form.Controls.Add($btn_exportspecs)

$btn_exportvmtools = New-Object System.Windows.Forms.Button
$btn_exportvmtools.Location = New-Object System.Drawing.Point (330, 110)
$btn_exportvmtools.Size = New-Object System.Drawing.Size(150,23)
$btn_exportvmtools.Text = "Export VM Tools"
$btn_exportvmtools.Add_Click({export_vmtools})
$main_form.Controls.Add($btn_exportvmtools)

$lbl_createsnaptasks = New-Object System.Windows.Forms.Label
$lbl_createsnaptasks.Location = New-Object System.Drawing.Point(10, 175)
$lbl_createsnaptasks.Text = "Snapshot Email Notification: "
$lbl_createsnaptasks.AutoSize = $true
$main_form.Controls.Add($lbl_createsnaptasks)

$txt_createsnaptasks = New-Object System.Windows.Forms.TextBox
$txt_createsnaptasks.Location = New-Object System.Drawing.Point(160, 170)
$txt_createsnaptasks.Width = 200
$txt_createsnaptasks.Text = "postgrads-sysadm@lists.stanford.edu"
$txt_createsnaptasks.AutoSize = $true
$main_form.Controls.Add($txt_createsnaptasks)

$btn_createsnaptasks = New-Object System.Windows.Forms.Button
$btn_createsnaptasks.Location = New-Object System.Drawing.Point (10, 200)
$btn_createsnaptasks.Size = New-Object System.Drawing.Size(150,23)
$btn_createsnaptasks.Text = "Create Snapshot Tasks"
$btn_createsnaptasks.Add_Click({create_snaptasks})
$main_form.Controls.Add($btn_createsnaptasks)

$txt_results = New-Object System.Windows.Forms.TextBox
$txt_results.Multiline = $true
$txt_results.Location = New-Object System.Drawing.Point(200,240)
$txt_results.Size = New-Object System.Drawing.Size(350,400)
$txt_results.Scrollbars = 3 #Scrollbars.Vertical
$txt_results.ReadOnly = $true
$main_form.Controls.Add($txt_results)

$btn_getsnapshots = New-Object System.Windows.Forms.Button
$btn_getsnapshots.Location = New-Object System.Drawing.Point (10, 265)
$btn_getsnapshots.Size = New-Object System.Drawing.Size(150,23)
$btn_getsnapshots.Text = "Get Snapshots"
$btn_getsnapshots.Add_Click({get_snapshots})
$main_form.Controls.Add($btn_getsnapshots)

$btn_delsnapshots = New-Object System.Windows.Forms.Button
$btn_delsnapshots.Location = New-Object System.Drawing.Point (10, 290)
$btn_delsnapshots.Size = New-Object System.Drawing.Size(150,23)
$btn_delsnapshots.Text = "Delete Snapshots"
$btn_delsnapshots.Add_Click({del_snapshots})
$main_form.Controls.Add($btn_delsnapshots)

$btn_getsnaptasks = New-Object System.Windows.Forms.Button
$btn_getsnaptasks.Location = New-Object System.Drawing.Point (10, 360)
$btn_getsnaptasks.Size = New-Object System.Drawing.Size(150,23)
$btn_getsnaptasks.Text = "Get Snapshot Tasks"
$btn_getsnaptasks.Add_Click({get_snaptasks})
$main_form.Controls.Add($btn_getsnaptasks)

$btn_delsnaptasks = New-Object System.Windows.Forms.Button
$btn_delsnaptasks.Location = New-Object System.Drawing.Point (10, 385)
$btn_delsnaptasks.Size = New-Object System.Drawing.Size(150,23)
$btn_delsnaptasks.Text = "Delete Snapshot Tasks"
$btn_delsnaptasks.Add_Click({del_snaptasks})
$main_form.Controls.Add($btn_delsnaptasks)

$main_form.ShowDialog()

