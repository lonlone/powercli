﻿#Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm 
Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $true

Connect-VIServer -Server asvcprd03.stanford.edu

Get-VM | Get-Snapshot | Select-Object vm,name,description,created,sizegb | Export-Csv -Path C:\temp\GetSnapshots.csv -NoTypeInformation -UseCulture

Disconnect-VIServer -Server asvcprd03.stanford.edu -Confirm:$false
Start-Sleep -s 60